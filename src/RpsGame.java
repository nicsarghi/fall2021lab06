//Nicoleta Sarghi
package src;
import java.util.Random;

public class RpsGame{
    private int wins = 0;
    private int ties = 0;
    private int losses = 0;
    private Random random;
    public RpsGame(){
        random = new Random();
    }
    
    public int getLosses() {
        return losses;
    }
    public int getTies() {
        return ties;
    }
    public int getWins() {
        return wins;
    }
    public String playRound(RpsChoiceType playerChoice){
        int computerChoiceIndex = random.nextInt(RpsChoiceType.values().length);
        if(playerChoice==null){
            throw new IllegalArgumentException("Invalid RPS type");
        }
        RpsChoiceType computerChoice = RpsChoiceType.values()[computerChoiceIndex];
        // check if its a tie
        if(playerChoice==computerChoice){
            ties++;
            return "No one wins.";
        }else{ // if not, find who wins
            boolean playerWon = playerWins(playerChoice,computerChoice);
            if(playerWon){
                wins++;
            }else{
                losses++;
            }
            return getWhoWon(playerChoice, computerChoice, playerWon);
        }

    }
    private String getWhoWon(RpsChoiceType playerChoice, RpsChoiceType computerChoice, boolean playerWon){
        if(playerWon){
            return "Player plays "+playerChoice.toString()+" and wins against "+computerChoice.toString() ;
        }else{
            return "Computer plays "+computerChoice.toString()+" and wins against "+playerChoice.toString(); 
        }
        
    }
    // returns true if player wins
    private boolean playerWins(RpsChoiceType playerChoice, RpsChoiceType computerChoice){
        boolean winCondition1 = playerChoice == RpsChoiceType.Paper && computerChoice == RpsChoiceType.Rock;
        boolean winCondition2 = playerChoice == RpsChoiceType.Scissors && computerChoice == RpsChoiceType.Paper;
        boolean winCondition3 = playerChoice == RpsChoiceType.Rock && computerChoice == RpsChoiceType.Scissors;
        return winCondition1 || winCondition2 || winCondition3;
    }
}