// Nicoleta Sarghi
package src;
import javafx.event.*;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>  {
    private RpsChoiceType playerChoice;
    private TextField message;
    private TextField winCount;
    private TextField lossCount;
    private TextField tieCount;
    private RpsGame rpsGame;

    public RpsChoice(RpsChoiceType playerChoice, TextField message, TextField winCount, 
                        TextField lossCount, TextField tieCount, RpsGame rpsGame) {
        this.playerChoice = playerChoice;
        this.message = message;
        this.winCount = winCount;
        this.lossCount = lossCount;
        this.tieCount = tieCount;
        this.rpsGame = rpsGame;
    }

    public void handle(ActionEvent arg0) {
        String result = rpsGame.playRound(playerChoice);
        message.setText(result);
        winCount.setText("Wins: "+rpsGame.getWins());
        lossCount.setText("Losses: "+rpsGame.getLosses());
        tieCount.setText("Ties: "+rpsGame.getTies());
    }
}
