// Nicoleta Sarghi
//2039804
package src;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {	
    private RpsGame rpsGame = new RpsGame();
    // these are fields so that there isnt a conflict when its being ordered
    private TextField message;
    private TextField winCount;
    private TextField lossCount;
    private TextField tieCount;
    private Button rockButton;
    private Button scissorsButton;
    private Button paperButton;

	public void start(Stage stage) {
		Group root = new Group(); 
        // add the overall vbox
        VBox overall = new VBox();
        root.getChildren().add(overall);
        //add ui
        addTopButtons(overall);
        addInfoTextFields(overall);
        listenToButtons();

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
    // add the buttons hbox
    private void addTopButtons(VBox overall){
        // init hbox container
        HBox buttons = new HBox();
        overall.getChildren().add(buttons);
        //add buttons
        rockButton = new Button("Rock");
        scissorsButton = new Button("Scissors");
        paperButton = new Button("Paper");
        buttons.getChildren().addAll(rockButton,scissorsButton,paperButton);
    }
    // set the on action events for the buttons
    private void listenToButtons(){
        rockButton.setOnAction(new RpsChoice(RpsChoiceType.Rock,message,winCount,lossCount,tieCount,rpsGame));
        scissorsButton.setOnAction(new RpsChoice(RpsChoiceType.Scissors,message,winCount,lossCount,tieCount,rpsGame));
        paperButton.setOnAction(new RpsChoice(RpsChoiceType.Paper,message,winCount,lossCount,tieCount,rpsGame));
    }   
    // add the info text field hbox and its children
    private void addInfoTextFields(VBox overall){// init hbox container
        HBox infoBox = new HBox();
        overall.getChildren().add(infoBox);
        message = addTextBoxToHBox("Welcome!",infoBox,350); // increase to 350 to show the message
        // rest of the boxes are smaller
        winCount = addTextBoxToHBox("Wins: 0",infoBox,100);
        lossCount = addTextBoxToHBox("Losses: 0",infoBox,100);
        tieCount = addTextBoxToHBox("Ties: 0",infoBox,100);
    }
    // helper method to add text boxes
    private TextField addTextBoxToHBox(String textInformation, HBox parentBox, int width){
        TextField text = new TextField(textInformation);
        text.setEditable(false); // you dont want the textfield to be edited
        text.setPrefWidth(width);
        parentBox.getChildren().add(text);
        return text;
    }
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
