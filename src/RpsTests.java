// Nicoleta Sarghi
package src;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class RpsTests {
    @Test
    public void TestRock(){
        RpsGame rpsGame = new RpsGame();
        String output = rpsGame.playRound(RpsChoiceType.Rock);
        boolean match = false;
        if (rpsGame.getTies()>0){
            System.out.println("Tied");
        }else{
            System.out.println("ROCK TEST");
            System.out.println(output);

            // check counter
            String matchString = rpsGame.getLosses()>0? "Computer" : "Player";
            match = output.contains(matchString);
            assertEquals(true,match);

            // check actual loss/win
            match = rpsGame.getLosses()>0? output.contains("Paper") : output.contains("Scissors");
            assertEquals(true,match);
        }
    }
    @Test
    public void TestScissors(){
        RpsGame rpsGame = new RpsGame();
        String output = rpsGame.playRound(RpsChoiceType.Scissors);
        boolean match = false;
        if (rpsGame.getTies()>0){
            System.out.println("Tied");
        }else{
            System.out.println("SCISSORS TEST");
            System.out.println(output);
            // check actual loss/win
            match = rpsGame.getLosses()>0? output.contains("Rock") : output.contains("Paper");
            assertEquals(true,match);
        }
    }
    @Test
    public void TestPaper(){
        RpsGame rpsGame = new RpsGame();
        String output = rpsGame.playRound(RpsChoiceType.Paper);
        boolean match = false;
        if (rpsGame.getTies()>0){
            System.out.println("Tied");
        }else{
            System.out.println("PAPER TEST");
            System.out.println(output);
            // check actual loss/win
            match = rpsGame.getLosses()>0? output.contains("Scissors") : output.contains("Rock");
            assertEquals(true,match);
        }
    }
    
}
