// Nicoleta Sarghi
package src;

public enum RpsChoiceType {
    Rock,
    Paper,
    Scissors
}
